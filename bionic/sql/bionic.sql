-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 14, 2014 at 11:53 AM
-- Server version: 5.5.33
-- PHP Version: 5.4.4-14+deb7u7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bionic`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` text NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `reference` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `article`, `views`, `reference`) VALUES
(1, 'Perfection is achieved, not when there is nothing more to add, but when there is nothing left to take away.', 0, 'Antoine de Saint-Exupéry'),
(2, 'The third-rate mind is only happy when it is thinking with the majority. The second-rate mind is only happy when it is thinking with the minority. The first-rate mind is only happy when it is thinking.', 0, 'A. A. Milne'),
(3, 'You''re ready for our advanced class.', 0, 'http://abstrusegoose.com/511'),
(4, 'img', 0, 'http://www.redheadsrule.me/content/4809/photo.jpg'),
(5, 'Hello!', 0, 'https://www.google.co.uk/search?q=hello&aq=f&oq=hello&aqs=chrome.0.57j0j5j0.1070j0&sourceid=chrome&ie=UTF-8'),
(6, 'DOMElement', 0, 'http://www.php.net/manual/en/domelement.setattribute.php');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL,
  `class` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `element_id` int(11) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `element_id` (`element_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `element_id`, `content`) VALUES
(1, 3, 'Bionic'),
(2, 9, 'home'),
(3, 10, 'test'),
(4, 11, 'link'),
(5, 12, 'more'),
(6, 13, 'stuff'),
(7, 7, 'OiS-CIS'),
(8, 14, '[table:articles]'),
(9, 24, '[field:article]'),
(10, 27, '[field:views]'),
(11, 26, '[field:reference]');

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `par_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `tag_id`, `par_id`) VALUES
(1, 48, 0),
(2, 45, 1),
(3, 102, 2),
(4, 13, 1),
(5, 29, 4),
(6, 46, 5),
(7, 39, 6),
(8, 66, 5),
(9, 1, 8),
(10, 1, 8),
(11, 1, 8),
(12, 1, 8),
(13, 1, 8),
(14, 60, 5),
(15, 5, 14),
(22, 37, 5),
(23, 5, 0),
(24, 84, 23),
(25, 37, 23),
(26, 1, 25),
(27, 88, 25);

-- --------------------------------------------------------

--
-- Table structure for table `element_styles`
--

CREATE TABLE IF NOT EXISTS `element_styles` (
  `element_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  KEY `class_id` (`element_id`,`style_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `element_styles`
--

INSERT INTO `element_styles` (`element_id`, `style_id`) VALUES
(1, 1),
(1, 2),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(5, 8),
(5, 10),
(5, 11),
(5, 12),
(6, 1),
(6, 13),
(6, 14),
(7, 20),
(7, 21),
(7, 22),
(8, 1),
(8, 15),
(8, 16),
(14, 1),
(14, 13),
(14, 17),
(14, 31),
(22, 1),
(22, 18),
(22, 19),
(24, 1),
(24, 35),
(24, 37),
(25, 1),
(25, 36),
(26, 9),
(26, 25),
(27, 38),
(27, 40);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE IF NOT EXISTS `properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property` text NOT NULL,
  `values` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `property`, `values`) VALUES
(1, 'width', 'auto|length|%|initial|inherit'),
(2, 'height', 'auto|length|%|initial|inherit'),
(3, 'padding', 'length|initial|inherit'),
(4, 'margin', 'length|auto|initial|inherit'),
(5, 'position', 'static|absolute|fixed|relative|initial|inherit'),
(6, 'font-family', 'sans-serif|serif|monospace|initial|inherit'),
(7, 'box-sizing', 'content-box|border-box|initial|inherit'),
(8, 'min-width', 'length|initial|inherit'),
(9, 'max-width', 'length|initial|inherit'),
(10, 'min-height', 'length|initial|inherit'),
(11, 'max-height', 'length|initial|inherit'),
(12, 'box-shadow', 'none|h-shadow v-shadow blur spread color |inset|initial|inherit'),
(13, 'background-color', 'color|transparent|initial|inherit'),
(14, 'color', 'color|initial|inherit'),
(15, 'font-size', 'medium|xx-small|x-small|small|large|x-large|xx-large|smaller|larger|length|initial|inherit'),
(16, 'display', 'inline|block|flex|inline-block|inline-flex|inline-table|list-item|run-in|table|table-caption|table-column-group|table-header-group|table-footer-group|table-row-group|table-cell|table-column|table-row|none|initial|inherit'),
(17, 'float', 'none|left|right|initial|inherit'),
(18, 'cursor', 'url|alias|all-scroll|auto|cell|context-menu|col-resize|copy|crosshair|default|e-resize|ew-resize|help|move|n-resize|ne-resize|nesw-resize|ns-resize|nw-resize|nwse-resize|no-drop|none|not-allowed|pointer|progress|row-resize|s-resize|se-resize|sw-resize|text|vertical-text|w-resize|wait|zoom-in|zoom-out|initial|inherit'),
(19, 'overflow', 'visible|hidden|scroll|auto|initial|inherit');

-- --------------------------------------------------------

--
-- Table structure for table `selectors`
--

CREATE TABLE IF NOT EXISTS `selectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `selector` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `selectors`
--

INSERT INTO `selectors` (`id`, `selector`) VALUES
(1, 'body *'),
(2, 'nav a'),
(3, 'nav a:hover'),
(4, 'main article');

-- --------------------------------------------------------

--
-- Table structure for table `selector_styles`
--

CREATE TABLE IF NOT EXISTS `selector_styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `selector_id` int(11) NOT NULL,
  `style_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `style_id` (`style_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `selector_styles`
--

INSERT INTO `selector_styles` (`id`, `selector_id`, `style_id`) VALUES
(1, 1, 3),
(2, 1, 4),
(3, 1, 5),
(4, 1, 6),
(5, 1, 7),
(6, 2, 23),
(7, 2, 24),
(8, 2, 25),
(9, 2, 2),
(10, 2, 26),
(11, 2, 27),
(12, 3, 28),
(13, 3, 29),
(14, 3, 30),
(15, 4, 32),
(16, 4, 33),
(17, 4, 34),
(18, 4, 18),
(19, 4, 25);

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE IF NOT EXISTS `structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `structures`
--

INSERT INTO `structures` (`id`, `name`) VALUES
(1, 'page'),
(2, 'article');

-- --------------------------------------------------------

--
-- Table structure for table `structure_elements`
--

CREATE TABLE IF NOT EXISTS `structure_elements` (
  `structure_id` int(11) NOT NULL,
  `element_id` int(11) NOT NULL,
  KEY `structure_id` (`structure_id`,`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `structure_elements`
--

INSERT INTO `structure_elements` (`structure_id`, `element_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `property_id`, `value`) VALUES
(1, 1, '100%'),
(2, 2, '100%'),
(3, 3, '0'),
(4, 4, '0'),
(5, 5, 'relative'),
(6, 6, 'sans-serif'),
(7, 7, 'border-box'),
(8, 4, 'auto'),
(9, 1, '80%'),
(10, 8, '800px'),
(11, 9, '1024px'),
(12, 12, '0 0 25px -5px #000'),
(13, 2, '150px'),
(14, 13, '#fff'),
(15, 2, '50px'),
(16, 13, '#def'),
(17, 10, '500px'),
(18, 2, '100px'),
(19, 13, '#012'),
(20, 3, '30px'),
(21, 15, '40px'),
(22, 14, '#48f'),
(23, 15, '20px'),
(24, 16, 'block'),
(25, 17, 'left'),
(26, 3, '10px 15px'),
(27, 4, '0 0 0 10px'),
(28, 14, '#fff'),
(29, 13, '#48f'),
(30, 18, 'pointer'),
(31, 3, '10px 50px'),
(32, 12, '0 2px 12px -5px #000'),
(33, 1, '23%'),
(34, 4, '1%'),
(35, 2, '75px'),
(36, 2, '25px'),
(37, 19, 'hidden'),
(38, 1, '20%'),
(39, 9, '100%'),
(40, 17, 'right');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=110 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag`) VALUES
(1, 'a'),
(2, 'abbr'),
(3, 'address'),
(4, 'area'),
(5, 'article'),
(6, 'aside'),
(7, 'audio'),
(8, 'b'),
(9, 'base'),
(10, 'bdi'),
(11, 'bdo'),
(12, 'blockquote'),
(13, 'body'),
(14, 'br'),
(15, 'button'),
(16, 'canvas'),
(17, 'caption'),
(18, 'cite'),
(19, 'code'),
(20, 'col'),
(21, 'colgroup'),
(22, 'command'),
(23, 'datalist'),
(24, 'dd'),
(25, 'del'),
(26, 'details'),
(27, 'dfn'),
(28, 'dialog'),
(29, 'div'),
(30, 'dl'),
(31, 'dt'),
(32, 'em'),
(33, 'embed'),
(34, 'fieldset'),
(35, 'figcaption'),
(36, 'figure'),
(37, 'footer'),
(38, 'form'),
(39, 'h1'),
(40, 'h2'),
(41, 'h3'),
(42, 'h4'),
(43, 'h5'),
(44, 'h6'),
(45, 'head'),
(46, 'header'),
(47, 'hr'),
(48, 'html'),
(49, 'i'),
(50, 'iframe'),
(51, 'img'),
(52, 'input'),
(53, 'ins'),
(54, 'kbd'),
(55, 'keygen'),
(56, 'label'),
(57, 'legend'),
(58, 'li'),
(59, 'link'),
(60, 'main'),
(61, 'map'),
(62, 'mark'),
(63, 'menu'),
(64, 'meta'),
(65, 'meter'),
(66, 'nav'),
(67, 'noscript'),
(68, 'object'),
(69, 'ol'),
(70, 'optgroup'),
(71, 'option'),
(72, 'output'),
(73, 'p'),
(74, 'param'),
(75, 'pre'),
(76, 'progress'),
(77, 'q'),
(78, 'rp'),
(79, 'rt'),
(80, 'ruby'),
(81, 's'),
(82, 'samp'),
(83, 'script'),
(84, 'section'),
(85, 'select'),
(86, 'smail'),
(87, 'source'),
(88, 'span'),
(89, 'strong'),
(90, 'style'),
(91, 'sub'),
(92, 'summary'),
(93, 'sup'),
(94, 'table'),
(95, 'tbody'),
(96, 'td'),
(97, 'textarea'),
(98, 'tfoot'),
(99, 'th'),
(100, 'thead'),
(101, 'time'),
(102, 'title'),
(103, 'tr'),
(104, 'track'),
(105, 'u'),
(106, 'ul'),
(107, 'var'),
(108, 'video'),
(109, 'wbr');

-- --------------------------------------------------------

--
-- Table structure for table `tag_property`
--

CREATE TABLE IF NOT EXISTS `tag_property` (
  `tag_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  UNIQUE KEY `property_tag` (`tag_id`,`property_id`),
  KEY `property_id` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='limit a property to a certain tag, else assume all tags';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contents`
--
ALTER TABLE `contents`
  ADD CONSTRAINT `contents_ibfk_1` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`);

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

--
-- Constraints for table `tag_property`
--
ALTER TABLE `tag_property`
  ADD CONSTRAINT `tag_property_ibfk_2` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  ADD CONSTRAINT `tag_property_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

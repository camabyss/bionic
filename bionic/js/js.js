function JShistory($o,h) {
	if (localStorage.JShistory===undefined) localStorage.JShistory="[]";
	if ($o=='back') {
		//pop and restore
		var history=JSON.parse(localStorage.JShistory),
			H=history.pop(),
			h=history.pop();
		localStorage.JShistory=JSON.stringify(history);
		JShistory('restore',h)
	} else if ($o=='restore') {
		if (h===undefined) {
			var history=JSON.parse(localStorage.JShistory),
			h=history.pop();
			if (h=='set restore') {
				H=history.pop();
				localStorage.JShistory=JSON.stringify(history);
				JShistory('restore',H)
			}
		} else {
			if (h['type']=='category') {
				JScategory(undefined,undefined,h['id'])
			} else if (h['type']=='article') {
				JSartpage(undefined,undefined,h['id'])
			}
		}
		
	} else {
		//push
		var history=JSON.parse(localStorage.JShistory);
		history.push($o)
		localStorage.JShistory=JSON.stringify(history);
		window.history.pushState($o);
	}
}
function JSload($o,i,result) {
	$.get('php/get.php?i='+JSON.stringify(i),function (data){
		$($o).html(data)
		JSresult(result)
	})
}
function JSreplace($o,i) {
	var _this=this,
		sets=[],
		done=false;
	if($o instanceof JSreplace) {
		$o.SET(i)
	} else{
		$.get('php/get.php?i='+JSON.stringify(i),function (data){
			var $data=$(data);
			$($o).after($data)
			$($o).remove()
			done=true
			_this.DONE($data)
		})
	}
	this.DONE=function ($p) {
		$o=$p;
		if (sets.length) {
			JSreplace($o,sets.pop())
		}
	}
	this.SET=function (j){
		if (done) {
			JSreplace($o,j)
		} else {
			sets.push(j)
		}
	}
}
function JSreload($o,r,result) {
	$("[db_code]").each(function () {
		var in_codes=false,
			db_code=JSON.parse($(this).attr('db_code')),
			$this=$(this);
		for (var dc in db_code) {
			for (var R in r) {
				var match=true
				for (var k in r[R]['find']) {
					if (!(k in db_code[dc]) || (k in db_code[dc] && db_code[dc][k]!=r[R]['find'][k])) {
						match=false
					}
				}
				if (match) {
					for (var k in r[R]['add']) {
						db_code[dc][k]=r[R]['add'][k]
					}
					in_codes=true;
					break;
				}
			}
		}
		if(in_codes){
			$.get('php/get.php?i='+JSON.stringify(db_code),function (dat) {
				$this.html(dat)
				JSresult(result)
			})
		}
	})
}
function JSrespond() {
	$.post('php/put.php',{'type':'test_response'},function (data) {
		console.log(data)
	})
}
function JSresult(i) {
	if (!(i===undefined)) {
		$.post('php/put.php',{'type':'result','result':i},function (data) {
			if (data=='reload') {
				JShistory('set restore')
				location.reload();
			}
		})
	}
}

function JSarttype($o,t) {
	var $art=$($o).closest('article')
	$art.find('a.selected').removeClass('selected')
	$($o).addClass('selected')
	if (t=='image') {
		$art.find('textarea')
			.attr('temp_val',$art.find('textarea').val())
			.val('img')
			.addClass('hidden')
		var $img=$art.find('span').detach()
		$art.find('textarea').after($img)
		$art.find('img')
			.attr('src',$art.find('input').val())
			
			.closest('span').removeClass('hidden')
	} else if (t=='text') {
		$art.find('textarea')
			.val($art.find('textarea').attr('temp_val'))
			.removeClass('hidden')
		$art.find('img')
			.closest('span').addClass('hidden')
	}
}
function JSsave($o,t,result) {
	if (t=='article') {
		var $art=$($o).closest('article'),
			$lj=new JSreplace($($o),[{"name":'loader'}]);
		$.post('php/post.php',{'article':$art.find('textarea').val(),'reference':$art.find('input').val()},function (data) {
			if ('error' in data) {
				JSreplace($lj,[{"name":"article_save"}])
				if (data['error']=='field') {
					$art.find('textarea').css('border-color','auto')
					$art.find('input').css('border-color','auto')
					for (var f in data['fields']) {
						if (data['fields'][f]=='article') {
							$art.find('textarea').css('border-color','#f00')
						} else {
							$art.find('input').css('border-color','#f00')
						}
					}
				}
			} else{
				JSreload(this,[{"find":{"type":"table","name":"articles","structure":"article"},"add":{"filter":$('nav a.selected').html()}},{"find":{"name":"article_add"}}],result)
			}
		},'json')
		//captchca
		//post & validate
		//refresh if all is gooood
		//else highlight bad things
	}
}
function JScategory($o,result,id) {
	if (!(id===undefined)) $o=$('nav a[db_reff='+id+']')
	JShistory({'type':'category','id':$($o).attr('db_reff')})
	$($o).siblings().removeClass('selected')
	$($o).addClass('selected')
	JSreload(this,[{"find":{"type":"table","name":'articles'},"add":{"filter":$($o).html()}},{"find":{"name":'article_add'}}],result)
}
function JSartpage($o,result,id) {
	id=(id===undefined ? $($o).closest('article').attr('db_reff') : id)
	$.post('php/put.php',{'type':'view','id':id},function (data) {
		console.log(data)
	})
	JShistory({'type':'article','id':id})
	JSload($('main'),[{"type":"table","name":"articles","structure":"article_page","where":[{'operator':'=','field':'id','value':id}]}],result)
}
$(function () {
	JSrespond();
	JShistory('restore');
	
})
window.onpopstate = function(e){
	if(e.state!=null){
		JShistory('back')
	}
};
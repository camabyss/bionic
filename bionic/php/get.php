<?php
if(isset($_GET['i'])) {
	include_once 'funct.php';
	$db=connect();
	$rm=connect('memory');
	$sess_id=session_id();
	$session_q=mysqli_query($rm,"SELECT id FROM sessions WHERE hash='$sess_id'");
	$session=$session_q->fetch_assoc();
	if ($session) {
		$session_id=$session['id'];
	} else {
		mysqli_query($rm,"INSERT INTO sessions (`hash`) VALUES ('$sess_id')");
		$session_id=(int) $rm->insert_id;
	}
	$test_q=mysqli_query($rm,"SELECT * FROM tests");
	$d=false;
	while ($test=$test_q->fetch_assoc()) {
		if ($test['name']!='default') {
			$done_q=mysqli_query($rm,"SELECT * FROM session_tests WHERE test_id='{$test['id']}' AND session_id='$session_id'");
			$done=$done_q->fetch_assoc();
			//insert if non-existant
			if (!$done) {
				mysqli_query($rm,"INSERT INTO session_tests (`test_id`, `session_id`) VALUES('{$test['id']}', '$session_id')");
				$done_q=mysqli_query($rm,"SELECT * FROM session_tests WHERE test_id='{$test['id']}' AND session_id='$session_id'");
				$done=$done_q->fetch_assoc();
			}
			// retest if not panic, not completed and not expected
			if (! complete($rm,$done['result'],$test['result_id'])) {
				$test_id=$test['id'];
				//switches for existing test
				set_switches($rm,$test['id']);
				$d=true;
				break;
			}
		}
	}
	if (!$d) {
		//administer new test
		$test_id=new_test($db,$rm);
		mysqli_query($rm,"INSERT INTO session_tests (`test_id`, `session_id`) VALUES('$test_id', '$session_id')");
		set_switches($rm,$test_id);
	}
	$dom=new DOMDocument();
	$page_styles=array();
	
	
	
	// elements
	if (isset($_GET['f'])) {
		$struc=create_structure($db,$rm,$dom,$dom,$_GET['i'],$_GET['f']);
	} else {
		$struc=create_structure($db,$rm,$dom,$dom,$_GET['i']);
	}
	mysqli_close($db);
	mysqli_close($rm);
	// styles
	foreach ($struc['styles'] as $select=>$s_style) {
		if (!(array_key_exists($select,$page_styles) && $page_styles[$select]==$s_style)) {
			$page_styles[$select]=$s_style;
		}
	}
	$style_str='';
	foreach($page_styles as $select=>$styles) {
		$style_str.=$select.' {'.$styles.'}';
	}
	$page_style=$dom->createElement('style',$style_str);
	$b_el=$dom->getElementsByTagName('head');
	if ($b_el->length) {
		$b_el->item(0)->appendChild($page_style);
		//panic-button
	} else {
		$b_el=$dom->documentElement->childNodes->item(0);
		$dom->documentElement->insertBefore($page_style,$b_el);
	}
	$d_el=$dom->getElementsByTagName('body');
	if ($d_el->length) {
		$panic=$dom->createElement('a','has something gone wrong?');
		$panic->setAttribute('ondblclick',"JSresult(8);");
		$panic->setAttribute('class',"panic");
		$panic->setAttribute('style',"position: fixed;
bottom: 10px;
right: 10px;
width: 120px;
padding: 8px;
border-radius: 5px;
border: 1px solid #a00;
font-size: 14px;
z-index: 10;
text-align: center;
box-shadow: inset -1px -1px 10px #830;
cursor:pointer;");
		$d_el->item(0)->appendChild($panic);
	}
	
	echo $dom->saveHTML();
}
?>


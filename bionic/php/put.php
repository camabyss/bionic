<?php
include_once 'funct.php';
//$db=connect();
if ($_POST['type']=='test_response') {
	$rm=connect('memory');
	$session_id=session_id();
	$session_test_q=mysqli_query($rm,"SELECT session_tests.id 
		FROM session_tests
		LEFT OUTER JOIN sessions ON session_tests.session_id = sessions.id
		WHERE sessions.hash =  '$session_id'
		AND response IS NULL");
	$session_test=$session_test_q->fetch_assoc();
	if ($session_test) {
		if (mysqli_query($rm,"UPDATE session_tests
			SET response=(SELECT UTC_TIMESTAMP())
			WHERE id = '{$session_test['id']}'")) {
				echo "responsive";
			}
	}
	 else {
	 	echo 'done?';
	 }
} else 
if ($_POST['type']=='result') {
	$rm=connect('memory');
	$session_id=session_id();
	//check if exists, increment by if not
	$session_test_q=mysqli_query($rm,"SELECT session_tests.id ,session_tests.result
		FROM session_tests
		LEFT OUTER JOIN sessions ON session_tests.session_id = sessions.id
		WHERE sessions.hash = '$session_id'
		ORDER BY id DESC");
	$session_test=$session_test_q->fetch_assoc();
	$result_q=mysqli_query($rm,"SELECT id FROM results WHERE result_n='{$_POST['result']}'");
	$result=$result_q->fetch_assoc();
	$res=false;
	if ($session_test['result']) {
		if (! (in_array($result['id'],results($rm,$session_test['result'])))) {
			$res=$session_test['result']+$_POST['result'];
		}
	} else {
		$res=$_POST['result'];
	}
	if ($res) {
		mysqli_query($rm,"UPDATE session_tests
			SET result='$res'
			WHERE id = '{$session_test['id']}'");
		if (complete($rm,$res,$result['id'])) {
			echo 'reload';
		}
	}
	
	
	//trigger reload if test complete
	//echo $_POST['result'];
} else
if ($_POST['type']=='view') {
	$db=connect();
	$article_q=mysqli_query($db,"SELECT views FROM articles WHERE id='{$_POST['id']}'");
	$article=$article_q->fetch_assoc();
	$views=$article['views']+1;
	mysqli_query($db,"UPDATE articles
		SET views='$views'
		WHERE id='{$_POST['id']}'");
}
?>
<?php
session_start();
include_once 'test.php';
include_once 'simplehtmldom/simple_html_dom.php';
require_once('recaptcha/recaptchalib.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
// A library of common, but not in-built functions
//http://stackoverflow.com/questions/6041741/fastest-way-to-check-if-a-string-is-json-in-php
function is_json($string) {
  $array = json_decode($string, true);
  return !empty($string) && is_string($string) && is_array($array) && !empty($array) && json_last_error() == 0;
}
function connect($db_name=false) {
	$db=mysqli_connect('localhost','root','tpm 1342',($db_name ? 'bionic_'.$db_name : 'bionic'));
	if ($db) return $db;
	else die('<error>No database connection</error>');
}
function pre_dump($obj) {
	echo "<pre>";
	var_dump($obj);
	echo "</pre>";
}

$defs=array(
	"type"=>"structure",
	"STRUC"=>array(
		"function"=>array(
			"this"=>"this",
			"arguments"=>array()
		),
		"table"=>array(
			"limit"=>"15",
			"where"=>array(),
			"order"=>array()
		)
	)
);
$defs=json_decode(json_encode($defs));
function structure_defaults ($db,$strucs) {
	global $defs;
	foreach ($strucs as $struc) {
		foreach ($defs as $d=>$def) {
			if ($d!="STRUC" && ! array_key_exists($d,$struc)) {
				$struc->$d=$def;
			}
		}
		if (array_key_exists($struc->type,$defs->STRUC)) {
			foreach ($defs->STRUC->{$struc->type} as $d=>$def) {
				if (! array_key_exists($d,$struc)) {
					$struc->$d=$def;
				}
			}
		}
	}
	return clean_structure($db,$strucs);
}
$switches=array();
function set_switches($rm,$test=1,$table='element_styles') {
	//switches
	global $switches;
	$switches_q=mysqli_query($rm,"SELECT switches.table_id
		FROM tests
		JOIN test_switches ON tests.id = test_switches.test_id
		LEFT OUTER JOIN switches ON test_switches.switch_id = switches.id
		WHERE tests.name =  'default'
		AND switches.table_name = '$table'");
	$switches_sup_q=mysqli_query($rm,"SELECT switches.table_id, switches.mode
		FROM tests
		JOIN test_switches ON tests.id = test_switches.test_id
		LEFT OUTER JOIN switches ON test_switches.switch_id = switches.id
		WHERE tests.id = '$test'
		AND switches.table_name = '$table'");
	$switches_t=array();
	while ($switch=$switches_q->fetch_assoc()) {
		$switches_t[]=$switch['table_id'];
	}
	while ($switch_sup=$switches_sup_q->fetch_assoc()) {
		if ($switch_sup['mode']=='1') {
			$switches_t[]=$switch_sup['table_id'];
		} elseif ($switch_sup['mode']=='-1') {
			if (in_array($switch_sup['table_id'],$switches_t)) {
				$sid=array_search($switch_sup['table_id'],$switches_t);
				array_splice($switches_t,$sid,1);
			}
		}
	}
	$switches[$table]="'".implode("', '",$switches_t)."'";
}
function create_structure($db,$rm,$dom,$element,$structures,$data=false) {
	global $switches;
	$els_list=array();
	$structure_styles=array();
	$add_tos=array();
	$ret='';
	if (! is_json($structures))return array('styles'=>$structure_styles,'elements'=>$els_list,'add_to'=>$add_tos);
	$structures=structure_defaults($db,json_decode($structures));
	foreach ($structures as $structure) {
		if ($structure->type=="structure") {
			//get elements by structure name
			if ($structure->name=='page') {
				// selector styles
				$selectors=mysqli_query($db,
				"SELECT * FROM selectors");
				while ($selector=$selectors->fetch_assoc()) {
					$selector_style='';
					$selector_styles=mysqli_query($db,"
						SELECT properties.property, styles.value
						FROM selector_styles
						LEFT OUTER JOIN styles ON styles.id = selector_styles.style_id
						LEFT OUTER JOIN properties ON styles.property_id = properties.id
						WHERE selector_styles.selector_id={$selector['id']};");
					while ($sel_style=$selector_styles->fetch_assoc()) {
						$selector_style.=$sel_style['property'].':'.$sel_style['value'].';';
					}
					$structure_styles[$selector['selector']]=$selector_style;
					//$page_styles$selector_style;
				}
			}
			$els=mysqli_query($db,
				"SELECT elements.id, tags.tag, contents.content, elements.par_id
				FROM structures
				LEFT OUTER JOIN structure_elements ON structure_elements.structure_id = structures.id
				LEFT OUTER JOIN elements ON structure_elements.element_id = elements.id
				LEFT OUTER JOIN contents ON contents.element_id = elements.id
				INNER JOIN tags ON elements.tag_id = tags.id
				WHERE structures.name =  '{$structure->name}'
				ORDER BY elements.id ASC;");
			if ($els) {
				//for element
				while ($row=$els->fetch_assoc()) {
					// element styles
					$styles=mysqli_query($db,
						"SELECT properties.property, styles.value FROM element_styles
						LEFT OUTER JOIN styles
						ON styles.id=element_styles.style_id
						LEFT OUTER JOIN properties
						ON styles.property_id=properties.id
						WHERE element_styles.element_id={$row['id']} AND element_styles.id IN ({$switches['element_styles']});");
					//only if in default
					$el_style='';
					while ($style=$styles->fetch_assoc()) {
						$el_style.=$style['property'].':'.$style['value'].';';
					}
					if (! array_key_exists("#el_{$row['id']}",$structure_styles) && $el_style) $structure_styles["#el_{$row['id']}"]=$el_style;
					
					// element content
					$cont=$row['content'];
					if (is_json($cont)) {
						$el=$dom->createElement($row['tag'],'');
						$el->setAttribute('db_code', $cont);
						
						// get inner structures
						$a_cont=json_decode($cont);
						foreach ($a_cont as $a_c) {
							if (array_key_exists('data',$structure)){
								$a_c->data=$structure->data;
							}
						}
						$add_tos[]=array('element'=>$el,'structures'=>json_encode($a_cont));
					} else {
						$el=$dom->createElement($row['tag'], $cont);
					}
					
					// reff attributes
					$el->setAttribute("id","el_".$row['id']);
					if (array_key_exists('data',$structure) && $structure->data && array_key_exists('id',$structure->data)) {
						$el->setAttribute('db_reff',$structure->data->id);
					}
					
					// element events
					$events=mysqli_query($db,
						"SELECT events.event, element_events.action FROM element_events
						LEFT OUTER JOIN events
						ON events.id=element_events.event_id
						WHERE element_events.element_id={$row['id']};");
					while($event=$events->fetch_assoc()) {
						if (is_json($event['action'])) {
							
							//get function
							$add_tos[]=array('element'=>$el,'structures'=>$event['action'],'attribute'=>$event['event']);
						} else {
							$el->setAttribute($event['event'],$event['action']);
						}
					}
					
					// element attributes
					$attrs=mysqli_query($db,
						"SELECT attributes.attribute, element_attributes.value
						FROM element_attributes
						LEFT OUTER JOIN attributes ON attributes.id = element_attributes.attribute_id
						WHERE element_attributes.element_id={$row['id']};");
					while($attr=$attrs->fetch_assoc()) {
						$el->setAttribute($attr['attribute'],$attr['value']);
					}
					$els_list[$row['id']]=$el;
					if ($row['par_id']==0) {
						$element->appendChild($el);
					} else {
						$els_list[$row['par_id']]->appendChild($el);
					}
				}
			}
		} else if ($structure->type=="table") {
				$filts=false;
				//$filters=array('order'=>array(),'where'=>array());
				if (array_key_exists('filter',$structure) && $structure->filter) {
					$filts=mysqli_query($db,
					"SELECT action FROM categories
					WHERE category='{$structure->filter}';");
					while ($filt=$filts->fetch_assoc()) {
						if (is_json($filt['action'])) {
							$fas=json_decode($filt['action']);
							foreach ($fas as $fa) {
								if (array_key_exists('order',$fa)) {
									foreach ($fa->order as $fo) {
										$structure->order[]=$fo;
									}
								}
								if (array_key_exists('where',$fa)) {
									foreach ($fa->where as $fw) {
										$structure->where[]=$fw;
									}
								}
							}
						}
					}
				}
				$query="SELECT * FROM {$structure->name}";
				if (count($structure->where)) {
					$query.=' WHERE ';
					foreach ($structure->where as $w=>$wh) {
						if ($w!=0) $query.=' AND ';
						$query.=$wh->field.' '.$wh->operator.' \''.$wh->value.'\'';
					}
				}
				if (count($structure->order)) {
					$query.=' ORDER BY ';
					foreach ($structure->order as $o=>$order) {
						if ($o!=0) $query.=',';
						$query.=$order->field.' '.$order->direction;
					}
				}
				$query.=" LIMIT 0, {$structure->limit}";
				$recs=mysqli_query($db,
				$query);
				if ($recs) {
					while ($rec=$recs->fetch_assoc()) {
						//link from db
						$add_tos[]=array("element"=>$element,"structures"=>'[{"type":"structure","name":"'.$structure->structure.'","data":'.json_encode($rec).'}]');
						//ad_tos
						
						// $article=create_structure($db,$rm,$dom,$add_to['element'],'article',$art);
						// //if (! array_key_exists("#el_{$row['id']}",$structure_styles)) $structure_styles["#el_{$row['id']}"]=$el_style;
						// foreach ($article['styles'] as $id=>$art_styles) {
						// 	if (!(array_key_exists($id,$structure_styles) && $structure_styles[$id]==$art_styles)) {
						// 		$structure_styles[$id]=$art_styles;
						// 	}
						// }
					}
				}
			// } else if ($add_to['id']=='categories') {
			// 	$arts=mysqli_query($db,
			// 	"SELECT * FROM categories;");
			// 	while ($art=$arts->fetch_assoc()) {
			// 		$article=create_structure($db,$rm,$dom,$add_to['element'],'category_link',$art);
			// 		//if (! array_key_exists("#el_{$row['id']}",$structure_styles)) $structure_styles["#el_{$row['id']}"]=$el_style;
			// 		foreach ($article['styles'] as $id=>$art_styles) {
			// 			if (!(array_key_exists($id,$structure_styles) && $structure_styles[$id]==$art_styles)) {
			// 				$structure_styles[$id]=$art_styles;
			// 			}
			// 		}
			// 	}
			// }
			
		} else if ($structure->type=="field") {
			if ($structure->name=='article') {
				if (is_url($structure->data->reference)) {
					if ($structure->data->{$structure->name}=='img') {
						$img=$dom->createElement('img', '');
						$img->setAttribute("src",$structure->data->reference);
						$element->appendChild($img);
					} else {
						$a=$dom->createElement('a', $structure->data->{$structure->name});
						$a->setAttribute("href",$structure->data->reference);
						$element->appendChild($a);
					}
				} else {
					$p=$dom->createElement('p', $structure->data->{$structure->name});
					$element->appendChild($p);
				}
			} else if ($structure->name=='reference' && is_url($structure->data->{$structure->name})) {
				$reff=$structure->data->{$structure->name};
				// remove http/s/www
				$reff=preg_replace('/http(s)?\:\/\/(www.)?/','',$reff);
				// remove too many /fol/ders/
				$parts=explode('/',$reff);
				if (count($parts)>2) {
					$reff=array_slice($parts,0,1)[0];
					$reff.='.../'.implode('/',array_slice($parts,-2));
				}
				// remove get vars
				$gets=explode('?',$reff);
				if (count($gets)>1) {
					$reff=$gets[0];
					$reff.='?'.substr($gets[1],0,4).'...';
				}
				$element->setAttribute("href",$structure->data->{$structure->name});
				$val=$dom->createTextNode($reff);
				$element->appendChild($val);
			} else {
				$TN=$dom->createTextNode($structure->data->{$structure->name});
				$element->appendChild($TN);
			}
		} else if ($structure->type=="function") {
			$args='';
			if (count($structure->arguments)) {
				foreach ($structure->arguments as $argument) {
					$args.=','.json_encode($argument);
				}
			}
			$ret.="JS{$structure->name}({$structure->this}$args);";
		}
	}
	if ($ret) return $ret;
	foreach($add_tos as $at=>$add_to) {
		$struc=create_structure($db,$rm,$dom,$add_to['element'],$add_to['structures']);
		if (array_key_exists('attribute',$add_to)) {
			$add_to['element']->setAttribute($add_to['attribute'],$struc);
		} else {
			foreach ($struc['styles'] as $id=>$struc_styles) {
				if (!(array_key_exists($id,$structure_styles) && $structure_styles[$id]==$struc_styles)) {
					$structure_styles[$id]=$struc_styles;
				}
			}
		}
	}
	return array('styles'=>$structure_styles,'elements'=>$els_list,'add_to'=>$add_tos);
}
function clean_structure($db,$struc) {
	if (is_string($struc)) {
		return clean_data($db,$struc);
	} elseif (is_array($struc)) {
		foreach ($struc as $s=>$st) {
			if ($s!='data') $struc[$s]=clean_structure($db,$st);
		}
	} elseif (is_object($struc)) {
		foreach ($struc as $s=>$st) {
			if ($s!='data') $struc->$s=clean_structure($db,$st);
		}
	}
	return $struc;
}
function is_url($str) {
	return preg_match('/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/',
						$str);
}
//http://www.phpdevtips.com/2011/06/simplified-data-sanitizing/
function clean_data($db,$input) {
	$input = trim(htmlentities(strip_tags($input,",")));
	if (get_magic_quotes_gpc()) $input = stripslashes($input);
	$input = mysqli_real_escape_string($db,$input);
	return $input;
}
?>
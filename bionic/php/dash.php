<?php
include_once 'funct.php';
$db=connect();
$get_reffs=array(
	'structure_id'=>array('table'=>'structures','fields'=>array('name')),
	'property_id'=>array('table'=>'properties','fields'=>array('property')),
	'style_id'=>array('table'=>'styles','fields'=>array('property_id','value')),
	'tag_id'=>array('table'=>'tags','fields'=>array('tag')),
	'element_id'=>array('table'=>'elements','fields'=>array('par_id','tag_id')),
	'par_id'=>array('table'=>'elements','fields'=>array('tag_id')),
	'selector_id'=>array('table'=>'selectors','fields'=>array('selector')),
	'event_id'=>array('table'=>'events','fields'=>array('event')),
	'attribute_id'=>array('table'=>'attributes','fields'=>array('attribute'))
	);
$id_reffs=array(
	'element'=>array('elements','element_id','par_id'),
	'tag'=>array('tags','tag_id'),
	'property'=>array('properties','property_id'),
	'style'=>array('styles','style_id'),
	'selector'=>array('selectors','selector_id'),
	'structure'=>array('structures','name','structure_id'),
	'content'=>array('contents','content_id'),
	'event'=>array('events','event_id'),
	'attribute'=>array('attributes','attribute_id')
	);
function idReff($str){
	global $db,$id_reffs,$get_reffs;
	foreach ($id_reffs as $reff=>$ids) {
		if (in_array($str,$ids)) return $reff;
	}
	return false;
}
function getReffs($column,$value){
	global $db,$id_reffs,$get_reffs;
	$ret=array();
	if (!($get_reffs[$column]['table']=='elements' && $value=='0')) {
		$reffs=mysqli_query($db,
			"SELECT ".implode(', ',$get_reffs[$column]['fields'])." FROM {$get_reffs[$column]['table']} WHERE id=$value"
			);
		while($reff=$reffs->fetch_assoc()) {
			foreach ($get_reffs[$column]['fields'] as $field) {
				if (array_key_exists($field,$get_reffs)) {
					$f_ret=getReffs($field,$reff[$field]);
					foreach ($f_ret as $f_r) {
						$ret[]=$f_r;
					}
					
				} else {
					$ret[]=$reff[$field];
				}
			}
		}
	}
	return $ret;
}
function showRows($table){
	global $db,$id_reffs,$get_reffs;
	$rows=mysqli_query($db,
	"SELECT * FROM $table"
	);
	$id_reff=idReff($table);
	while ($row=$rows->fetch_assoc()) {
		?><article class=row <?php if (array_key_exists('id',$row)) { echo " db_id={$row['id']} ";} if ($id_reff) { echo " db_reff=$id_reff ";} ?> ><?php
		foreach($row as $column=>$value) {
			if ($column!='id') {
				$col_reff=idReff($column);
				?><div class=column  <?php if ($col_reff) { echo "db_reff=$col_reff db_id=$value";} else { echo "db_val=$value";} ?> ><?php
				if (array_key_exists($column,$get_reffs)) {
					$reffs=getReffs($column,$value);
					//pre_dump($reffs);
					foreach ($reffs as $field) {
						?><div class=column ><?php
						echo $field;
						?></div><?php
					}
				} else {
					echo $value;
				}
				?></div><?php
			}
		}
		?><div class=delete_button ></div>
		</article><?php
	}
}
if (isset($_POST['mode'])) {
	$datas=json_decode($_POST['datas']);
	$cols=array();
	$vals=array();
	foreach ($datas as $col=>$val) {
		$cols[]=$col;
		$vals[]=$val;
	}
	if (count($cols) && count($vals)) {
		if ($_POST['mode']=='add') {
			$sql="INSERT INTO {$_POST['table']} (`".implode('`, `',$cols)."`) VALUES ('".implode("', '",$vals)."');";
		} else if ($_POST['mode']=='delete') {
			$sql="DELETE FROM {$_POST['table']} WHERE";
			foreach ($cols as $i=>$col) {
				if ($i>0) {
					$sql.=" AND";
				}
				$sql.=" ".$col."='".$vals[$i]."'";
			}
		}
		$ret=mysqli_query($db,$sql);
		showRows($_POST['table']);
	}
} else {
?>
<style>
html,body {
overflow:hidden;
}
	body,body * {
		margin:0;
		padding:0;
		position:relative;
		box-sizing:border-box;
		font-family:sans-serif;
	}
	section.table {
		float:left;
		width:245px;
		padding:10px;
		margin:10px;
		font-size:12px;
		box-shadow: 0 1px 7px -3px #000;
		height:162px;
	}
	section.table article {
		width:100%;
		float:left;
	}
	section.table article div {
		float: left;
		max-height: 22px;
		overflow: hidden;
		height: 100%;
		padding: 2px;
		border-bottom: 1px solid #eee;
		border-left: 1px solid #eee;
	}
	*:not(.column):not(.hover)>.column {
		background-color:#fff;
	}
	section.table article .column .column {
		width:50%;
		margin:0;
		border:none;
		white-space:nowrap;
	}
	section.table article .column .column:not(:first-child) {
		border-left:1px dotted #bbb !important;
	}
	.col {
		font-size:120%;
		text-align:center;
	}
	.rows {
		width: 100%;
		float: left;
		height: 100px;
		overflow: auto;
	}
	.rows.short {
		height:70px;
	}
	header * {
		float:left;
	}
	header h2 {
		width:50%;
		text-align:center;
		font-size:17px;
	}
	header .search {
		width:40%;
		border: 1px solid #ccc;
		padding: 0 5px;
	}
	header .add_button {
		width:10%;
	}
	.hover {
		background-color:#def !important;
	}
	#joins {
		position: absolute;
		top: 0;
		right: 0;
		width: 300px;
		background: #fff;
		height:100%;
		overflow:auto;
	}
	#tables {
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		background: #fff;
		padding-right:300px;
		height:100%;
	}
	.join h2{
		font-size:12px;
	}
	.add_button:before {
		content:"+";
	}
	.add_button {
		width: 10%;
		height: 20px;
		background: #dfe;
		text-align: center;
		font-size: 17px;
	}
	.add_button:hover {
		cursor:pointer;
	}
	.col input {
		width:100%;
		height:100%;
		border: 1px solid #ccc;
	}
	section.table article div.val_drop {
		width:100% ;
		height:100%;
		padding:0;
	}
	.row.ui-draggable-dragging {
		width:100px;
		height:30px;
		border:1px solid #000;
		background:#fff;
		overflow:hidden;
	}
	.row.ui-draggable-dragging div {
		float:left;
	}
	.ui-droppable.active {
		background:#ccf;
	}
	.ui-droppable.active_hover {
		background:#55f;
	}
	.hidden {
		height:0px !important;
	}
	.delete_button {
		width:10%;
		height:100%;
		background:#fed;
		text-align:center;
		cursor:pointer;
	}
	.delete_button:before {
		content:"x";
	}
	.cols {
		background-color:#f6f6f6;
	}
	.val_drop .delete_button {
	display:none;
	}
	.ui-draggable-dragging .delete_button {
	display:none;
	}
	/*http://stackoverflow.com/questions/880512/prevent-text-selection-after-double-click*/
	body *:not(input) {
		-webkit-user-select: none; /* webkit (safari, chrome) browsers */
		-moz-user-select: none; /* mozilla browsers */
		-khtml-user-select: none; /* webkit (konqueror) browsers */
		-ms-user-select: none; /* IE10+ */
	}
	#veil {
		position:absolute;
		top:0;
		left:0;
		height:100%;
		width:100%;
		background:#fff;
	}
	#veil.done {
		display:none;
	}
	.val_drop .column {
		padding: 0 !important;
	}
</style>
<script src=../js/jq.js></script>
<script src=../js/hi.js></script>
<script src=../js/st/lib/jquery-scrollto.js></script>
<script src=../js/ui/js/jquery-ui-1.10.4.custom.min.js ></script>
<div id=tables >
<?php
$dom=new DOMDocument();
$tables=mysqli_query($db,
"SHOW TABLES"
);
while ($table=$tables->fetch_assoc()) {
	$cols=mysqli_query($db,
	"SHOW columns FROM {$table['Tables_in_bionic']}"
	);
?><section class="table<?php if(strstr($table['Tables_in_bionic'],'_')) {echo " join";} ?>" db_table=<?php echo $table['Tables_in_bionic']; ?> >
	<header>
		<h2><?php echo $table['Tables_in_bionic']; ?></h2>
		<input placeholder=search class=search />
		<div class='add_button'></div>
	</header>
	<article class=cols ><?php
	while ($col=$cols->fetch_assoc()) {
		if ($col['Field']!='id') {
			$col_reff=idReff($col['Field']);
			?><div class=col <?php if ($col_reff) { echo "db_reff=$col_reff";} ?> ><?php
			echo ($col['Field']);
			?></div><?php
		}
	}
	?></article>
	<div class=rows><?php
	showRows($table['Tables_in_bionic']);
	?></div>
</section><?php
}
?>
</div>
<div id=joins ></div>
<div id=veil></div>
<script>
	function hover_events($obj) {
		if($obj===undefined) $obj=$('body')
		$obj.find('[db_reff][db_id]').hoverIntent(function () {
				var _this=this
				var $this=$(this)
				var $tables=$('section.table')
				$tables.each(function () {
					var $reffs=$(this).find('[db_reff='+$this.attr('db_reff')+'][db_id='+$this.attr('db_id')+']')
					$reffs.addClass('hover')
					if ($reffs.length && $reffs.eq(0)[0]!=_this && this!=$this.closest('.table')[0]) {
						$(this).find('.rows').clearQueue()
						$(this).find('.rows').stop(true)
						$(this).find('.rows').animate({
							scrollTop: $reffs.eq(0).offset().top-$(this).find('.rows .row').eq(0).offset().top}, 200);
					}
				})
				
			
		},function () {
			$('[db_reff='+$(this).attr('db_reff')+'][db_id='+$(this).attr('db_id')+']').removeClass('hover')
		})
	}
	function fill_widths($obj) {
		if($obj===undefined) $obj=$('body')
		$obj.find('article').each(function () {
			var cols=$(this).children('.column,.col')
			cols.css('width',(90/cols.length)+'%')
			cols.each(function () {
				var sub_cols=$(this).children('.column')
				sub_cols.css('width',(100/sub_cols.length)+'%')
			})
		})
	}
	function delete_events($obj) {
		if($obj===undefined) $obj=$('body')
		$obj.find('.delete_button').dblclick(function () {
			var $rows=$(this).closest('.rows');
			$.post('dash.php',{'table':$(this).closest('.table').attr('db_table'),'datas':JSON.stringify({'id':$(this).closest('article.row').attr('db_id')}),'mode':'delete'},function (data) {
				$rows.html(data)
				fill_widths($rows)
				delete_events($rows)
				hover_events($rows)
				//$rows.scrollTop($rows.prop('scrollHeight'))
				//console.log(data)
				//location.reload();
			})
		})
	}
	function add_button() {
		var $table=$(this).closest('.table'),
			$cols=$table.find('.cols .col'),
			$ins=$('<article/>',{'class':'inputs'}),
			$del=$('<div/>',{'class':'delete_button'});
		$table.find('.rows').addClass('short')
		$(this).unbind('click')
		$(this).click(function () {
			//try save
			var got_all=true,
				datas={};
			$ins.find('.col').each(function () {
				var vd=$(this).find('.val_drop'),
					ip=$(this).find('input')
				if (vd.length) {
					if (vd.is('[value]')) {
						datas[vd.attr('name')]=vd.attr('value')
					} else got_all=false
				} else if (ip.length){
					if (ip.val()) {
						datas[ip.attr('name')]=ip.val()
					} else got_all=false
				} else {
					got_all=false
				}
			})
			$.post('dash.php',{'table':$table.attr('db_table'),'datas':JSON.stringify(datas),'mode':'add'},function (data) {
				var $rows=$table.find('.rows')
				$rows.html(data)
				$rows.scrollTop($rows.prop('scrollHeight'))
				fill_widths($rows)
				delete_events($rows)
				hover_events($rows)
				//console.log(data)
				//location.reload();
			})
			$(".ui-draggable").draggable("destroy");
			$ins.remove()
			$(this).unbind('click')
			$(this).click(add_button)
			$table.find('.rows').removeClass('short')
		})
		$ins.insertAfter($table.find('.cols').eq(0))
		$cols.each(function () {
			var $cont=$('<div/>',{'class':'col'});
			//if not foreign id, input is text
			if ($(this).html().indexOf('_id') == -1) {
				var $inp=$('<input/>',{'type':'text','name':$(this).html()})
			// else input is type (d&d)
			} else {
				var $inp=$('<div/>',{'class':'val_drop','name':$(this).html()}).html($(this).attr('db_reff'))
				$('article[db_reff='+$(this).attr('db_reff')+']').draggable({
					'containment':'body',
					'appendTo':'body',
					'helper':'clone'
				})
				$inp.droppable({
					'accept':'article[db_reff='+$(this).attr('db_reff')+']',
					'activeClass':'active',
					'hoverClass':'active_hover',
					'drop':function (event,ui) {
						$(this).html(ui.draggable.html())
						$(this).attr('value',ui.draggable.attr('db_id'))
					}
				})
			}
			$ins.append($cont.append($inp))
		})
		
		$ins.append($del)
		var $_this=$(this)
		$del.dblclick(function () {
			$(this).unbind('dblclick')
			$_this.unbind('click')
			$_this.click(add_button)
			$(".ui-draggable").draggable("destroy");
			$ins.remove()
			$table.find('.rows').removeClass('short')
		})
		var cols=$table.find('.inputs .col')
		cols.css('width',(90/cols.length)+'%')
	}
	
	var $joins=$('.table.join').detach()
	$('#joins').append($joins)
	
	$('.search').each(function () {
		var $this=$(this)
		$this.keypress(function (e) {
			var code = e.keyCode || e.which;
			 if(code == 13) {
				var $val=$this.val()
				if ($val) {
					$this.closest('.table').find('.rows').each(function() {
						$(this).find('.row').addClass('hidden')
						$(this).find('.column').each (function () {
							if ($val && ($(this).text().indexOf($val) != -1 || $val==$(this).attr('db_id'))) {
								$(this).closest('.row').removeClass('hidden')
							}
						})
					})
				}else {
					$this.closest('.table').find('.rows .row').removeClass('hidden')
				}
			 }
		})
	})
	$('.add_button').click(add_button)
	fill_widths()
	delete_events()
	hover_events()
	$('#veil').addClass('done')
</script>
<?php }
mysqli_close($db);?>
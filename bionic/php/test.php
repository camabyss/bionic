<?php
//decide on what to change
	//pick structure/selector
	// (80,20)
	// (structure:80(style:50,tag:20,event:30),selector:20(style:100))
	// add/change/remove
	// change likelyhood based on past results
	// generate json sets & css sets
	// generate a set of changes to test
	
	$defaults=array(
		"color"=>array(15,15,15),
		"pixels"=>0,
		"percentage"=>0,
		"number"=>1,
		"time"=>0
		);
	$units=array(
		"pixels"=>array('post','px'),
		"percentage"=>array('post','%'),
		"color"=>array('pre','#'),
		"time"=>array('post','s'));
	$special=array('length','number','time','lengths','%','color');
	function css_values($l) {
		$lengths=explode(' ',$l);
		$ret_lengths=array();
		foreach ($lengths as $length) {
			// pixels
			if (preg_match('/[0-9]+px/',$length)) {
				$ret_lengths[]=array("value"=>(int) substr($length,0,-2),"type"=>"pixels");
			} else
			// percentage
			if (preg_match('/[0-9]+%/',$length)) {
				$ret_lengths[]=array("value"=>(int) substr($length,0,-1),"type"=>"percentage");
			} else
			// color
			if (preg_match('/[#][0-9a-fA-F]{3}/',$length)) {
				$rgb=str_split(substr($length,1));
				foreach ($rgb as $k=>$c) {
					if (is_numeric($c)) {
						$rgb[$k]=(int) $c;
					} else {
						$ord=ord($c);
						if ($ord<71) {
							$rgb[$k]=$ord-55;
						} else {
							$rgb[$k]=$ord-87;
						}
					}
				}
				$ret_lengths[]=array("type"=>"color","value"=>$rgb);
			} else {
				$ret_lengths[]=$length;
			}
		}
		return $ret_lengths;
	}
	function values_css($l) {
		global $units;
		$vals=array();
		if(is_array($l)) {
			if(array_key_exists('type',$l)) {
				$a=values_css(array($l));
				return $a;
			} else {
				foreach ($l as $L) {
					if (is_array($L)) {
						if ($L['type']=='color') {
							$val='';
							foreach ($L['value'] as $v) {
								if ($v>9) {
									$val.=chr($v+87);
								} else {
									$val.=$v;
								}
							}
						} else {
							$val=$L['value'];
						}
						if (array_key_exists($L['type'],$units)) {
							if ($units[$L['type']][0]=='pre') {
								$vals[]=$units[$L['type']][1].$val;
							} else if ($units[$L['type']][0]=='post') {
								$vals[]=$val.$units[$L['type']][1];
							}
						} else {
							$vals[]=$L['value'];
						}
					} else {
						$vals[]=$L;
					}
				}
			}
			return implode(' ',$vals);
			
		} else {
			return $l;
		}
	}
	function generate_from_value($db,$value) {
		global $defaults;
		if (strstr(' ',$value)) {
			$vals=explode(' ',$value);
			$r=array();
			foreach ($vals as $val) {
				$r[]=generate_from_value($db,$val);
			}
			return $r;
		} else
		if ($value=='lengths') {
			return array(generate_from_value($db,'length'));
		} else
		if ($value=='length') {
			$r=random_decision(array(3,1));
			if ($r==0) {
				return array("type"=>"pixels","value"=>$defaults['pixels']);
			} elseif ($r==1) {
				return array("type"=>"percentage","value"=>$defaults['percentage']);
			}
		} else
		if ($value=='%') {
			return array("type"=>"percentage","value"=>$defaults['percentage']);
		} else
		if (array_key_exists($value,$defaults)) {
			return array("type"=>$value,"value"=>$defaults[$value]);
		} else {
			//search properties
			$property_q=mysqli_query($db,
				"SELECT values FROM properties
				WHERE property='$value'");
			$property=$property_q->fetch_assoc();
			return generate_from_values($db,$property['values']);
		}
	}
	function generate_from_values($db,$values) {
		global $special;
		$vals=explode('|',$values);
		$probs=array();
		foreach ($vals as $v) {
			//select val (if special (lengths,length,number,sub-props,complex value) more likely)
			if (strstr(' ',$v) ||in_array($v,$special)) {
				$probs[]=8;
			} else {
				$probs[]=1;
			}
		}
		$r=random_decision($probs);
		if ($probs[$r]==1) {
			return $vals[$r];
		} else {
			return generate_from_value($db,$vals[$r]);
		}
	}
	function random_decision($prob) {
		/* uses a random number to make a decision
		if the list [10,4,6] is given, there is a
			50% chance of returning 0,
			20% chance of 1, and
			30% chance of returning 2 */

		// calculate max number
		$m=0;
		foreach($prob as $p) { $m+=$p; }
		
		// generate random number
		$r=mt_rand(0,$m-1);
		
		// compare to probabilities
		$c=0;
		foreach ($prob as $k=>$p) {
			$c+=$p;
			if ($c>$r) return $k;
		}
	}
	function increment($i) {
		$v=$i['value'];
		if (is_array($v)) {
			$p=array();
			foreach ($v as $V) {
				$p[]=1;
			}
			$r=random_decision($p);
			$inc=increment(array('value'=>$i['value'][$r]));
			$i['value'][$r]=$inc['value'];
		} else {
			if ($v==0) {
				$r=1;
			} else {
				$r=random_decision(array(1,1));
			}
			if ($r==0) {
				$i['value']-=1;
			} elseif ($r==1) {
				$i['value']+=1;
			}
		}
		return $i;
	}
	function results($rm,$n) {
		$rets=array();
		$results_q=mysqli_query($rm,"SELECT * 
			FROM results
			ORDER BY result_n DESC");
		while ($result=$results_q->fetch_assoc()) {
			if ($n>=$result['result_n']) {
				$rets[]=$result['id'];
				$n-=$result['result_n'];
			}
		}
		return $rets;
	}
	function complete($rm,$result,$result_id) {
		return ! ((! $result) || ($result<7 && (! in_array($result_id,results($rm,$result)))));
	}
	function decide_on($db,$rm,$d) {
		$r=random_decision(array(1,2));
		$add=")";
		if ($r==0) {
			$add="AND s_ts.result IS NOT NULL) ORDER BY s_ts.result DESC";
		}elseif ($r==1) {
			$add="AND s_ts.result IS NULL) ORDER BY RAND() LIMIT 1";
		}
		if ($d=='property_id' && $r==1){
			$q=mysqli_query($db,"SELECT id FROM properties ORDER BY RAND() LIMIT 1");
			$test=$q->fetch_assoc();
			return $test['id'];
		} else {
			$q=mysqli_query($db,"SELECT ts.name, el_s.element_id, el_s.style_id, ts.result_id, s_ts.result, st.property_id
				FROM  `bionic`.`elements` el
				RIGHT OUTER JOIN  `bionic`.`element_styles` el_s ON el.id = el_s.element_id
				LEFT OUTER JOIN  `bionic`.`styles` st ON el_s.style_id = st.id
				LEFT OUTER JOIN  `bionic_memory`.`switches` sw ON el_s.id = sw.table_id
				LEFT OUTER JOIN  `bionic_memory`.`test_switches` t_sw ON sw.id = t_sw.switch_id
				LEFT OUTER JOIN  `bionic_memory`.`tests` ts ON t_sw.test_id = ts.id
				LEFT OUTER JOIN  `bionic_memory`.`session_tests` s_ts ON ts.id = s_ts.test_id
				WHERE (
					(
						s_ts.result <8
						OR s_ts.result IS NULL
					)
					AND (
						sw.id IS NOT NULL
					)
					AND NOT (
						s_ts.id IS NOT NULL 
						AND s_ts.result IS NULL 
						AND s_ts.response IS NULL
					)
				$add");
			if ($r==0) {
				$el_probs=array();
				while ($test=$q->fetch_assoc()) {
					if (array_key_exists($test[$d],$el_probs)) {
						if ($test['result']) {
							$result=results($rm,$test['result']);
							$el_probs[$test[$d]]['results'][]=$result;
						}
					} else {
						if ($test['result']) {
							$result=results($rm,$test['result']);
							$el_probs[$test[$d]]=array('results'=>array($result));
						} else {
							$el_probs[$test[$d]]=array('results'=>array());
						}
					}
					$el_probs[$test[$d]]['result_id']=$test['result_id'];
				}
				$els=array();
				$probs=array();
				foreach ($el_probs as $el=>$results) {
					$els[]=$el;
					$el_probs[$el]['probability']=0;
					foreach ($results['results'] as $result) {
						foreach($result as $res) {
							if ($results['result_id'] && $results['result_id']==$res) {
								$el_probs[$el]['probability']+=2;
							} else {
								$el_probs[$el]['probability']+=1;
							}
						}
					}
					$probs[]=$el_probs[$el]['probability'];
				}
				return $els[random_decision($probs)];
			} else {
				$test=$q->fetch_assoc();
				return $test[$d];
			}
		}
	}
	function new_test($db,$rm) {
		$decision=array();
		//decide if continuing or dithering, if dithering result should be NULL
		
		//select element (favour those which have not been selected, or have good records)
		
		$decision['element']=decide_on($db,$rm,'element_id');
			
				//select add / remove / change (if no styles, force add)
				$count_q=mysqli_query($db,"SELECT COUNT( * ) 
					FROM element_styles
					WHERE element_styles.element_id = '{$decision['element']}'");
				$count=$count_q->fetch_assoc();
				if ((int)$count['COUNT( * )']) {
					$d=random_decision(array(1,1,8));
				} else {
					$d=0;
				}
				if ($d==0) {
					$decision['action']='add';
					//if add select property
					$prop_id=decide_on($db,$rm,'property_id');
					$property_q=mysqli_query($db,
						"SELECT * FROM properties
						WHERE id='$prop_id'");
					$property=$property_q->fetch_assoc();
					//generate value
					$decision['style']=array('property'=>$property['id'],'value'=>generate_from_values($db,$property['values']));
				} else
				if ($d==1) {
					$decision['action']='remove';
					//if remove select style
					$style_q=mysqli_query($db,"SELECT styles.id, properties.property, styles.value, properties.values
						FROM element_styles
						LEFT OUTER JOIN styles ON styles.id = element_styles.style_id
						LEFT OUTER JOIN properties ON properties.id = styles.property_id
						WHERE element_styles.element_id = '{$decision['element']}'
						ORDER BY RAND()
						LIMIT 1");
					$style=$style_q->fetch_assoc();
					$decision['style']=$style['id'];
				} else
				if ($d==2) {
					$decision['action']='change';
					//if change select style
					$style_q=mysqli_query($db,"SELECT styles.id, properties.property, styles.value, properties.values
						FROM element_styles
						LEFT OUTER JOIN styles ON styles.id = element_styles.style_id
						LEFT OUTER JOIN properties ON properties.id = styles.property_id
						WHERE element_styles.element_id = '{$decision['element']}'
						ORDER BY RAND()
						LIMIT 1");
					$style=$style_q->fetch_assoc();
					$decision['from']=$style['id'];
					$vals=explode('|',$style['values']);
						if (in_array($style['value'],$vals)) {
						//if value not special generate from values
							$decision['to']=generate_from_values($db,$style['values']);
						} else {
							$val=css_values($style['value']);
							if (in_array('lengths',$vals) && $val[0]['type']=='length') {
								//echo "len";
								//else if lengths add/remove/change
								$r=random_decision(array(1,1,8));
								$decision['to']=$val;
							} else if (count($val)>1) {
								//if multiple values choose and change
								$props=array();
								foreach ($val as $v) {
									$probs[]=1;
								}
								$r=random_decision($probs);
								$val[$r]=increment($val[$r]);
								$decision['to']=$val;
							} else {
								$decision['to']=array(increment($val[0]));
							}
						}
						
				}
		//
		
		// element name
			$element_q=mysqli_query($db,"SELECT t.tag, tt.tag, ttt.tag, tttt.tag, structures.name
				FROM elements el
				LEFT OUTER JOIN elements par ON el.par_id = par.id
				LEFT OUTER JOIN elements par_par ON par.par_id = par_par.id
				LEFT OUTER JOIN elements par_par_par ON par_par.par_id = par_par_par.id
				LEFT OUTER JOIN tags t ON el.tag_id = t.id
				LEFT OUTER JOIN tags tt ON par.tag_id = tt.id
				LEFT OUTER JOIN tags ttt ON par_par.tag_id = ttt.id
				LEFT OUTER JOIN tags tttt ON par_par_par.tag_id = tttt.id
				LEFT OUTER JOIN structure_elements ON el.id = structure_elements.element_id
				LEFT OUTER JOIN structures ON structure_elements.structure_id = structures.id
				WHERE el.id = '{$decision['element']}'");
			$element=$element_q->fetch_array();
			$el_name="<span class=element>";
			$c=0;
			foreach($element as $k=>$el) {
				if (is_numeric($k)) $c++;
			}
			foreach($element as $k=>$el) {
				if (is_numeric($k)) {
					if ($el==$element['name'] && $k==$c-1) {
						$el_name.=" in the <span class=structure >$el</span> ";
					} else if ($el) {
						if($k==0){
							$el_name.=" <span class='tag element_tag' >$el</span> ";
						} else {
							$el_name.=" &lt <span class=tag >$el</span> ";
						}
					}
				}
			}
			$el_name.="</span>";
		//
		//expected result
		$structure_q=mysqli_query($db,"SELECT structures.id
			FROM structures
			JOIN structure_elements ON structure_elements.structure_id = structures.id
			WHERE structure_elements.element_id =  '{$decision['element']}'");
		$structure=$structure_q->fetch_assoc();
		$r_id='NULL';
		if ($structure) {
			$result_q=mysqli_query($rm,"SELECT result_id
				FROM structure_results
				WHERE structure_id = '{$structure['id']}'");
			$result=$result_q->fetch_assoc();
			if ($result) {
				$r_id="'{$result['result_id']}'";
			}
		}
		
		// INSERT test
		$n_test=mysqli_query($rm,"INSERT INTO tests (`name`,`result_id`) VALUES ('".json_encode($decision)."', $r_id)");
		$test_id=$rm->insert_id;
		if ($decision['action']=="add") {
			//property
			$property_q=mysqli_query($db,"SELECT property from properties
				WHERE id='{$decision['style']['property']}'");
			$property=$property_q->fetch_array();
			//should the (property) of (element) be (value)
			echo "<div id=test_description hidden >could the <span class=property>".$property['property']."</span> of ".$el_name." be <span class=value>".values_css($decision['style']['value'])."</span>?</div>";
			// check if exists
			$check_q=mysqli_query($db,"SELECT * FROM styles WHERE property_id='{$decision['style']['property']}' AND value='".values_css($decision['style']['value'])."'");
			$checked=false;
			while ($check=$check_q->fetch_assoc()) { $checked=$check['id'];}
			// to bionic
			if (! $checked) {
				// else INSERT style
				mysqli_query($db,"INSERT INTO styles
					(`property_id`,`value`)
					VALUES ('{$decision['style']['property']}','".values_css($decision['style']['value'])."')");
				$checked=$db->insert_id;
			}
			// INSERT element_style
			mysqli_query($db,"INSERT INTO element_styles
				(`element_id`,`style_id`)
				VALUES ('{$decision['element']}','$checked')");
			$element_style=$db->insert_id;
			// to _memory
			// INSERT switch
			mysqli_query($rm,"INSERT INTO switches (`table_name`,`table_id`,`mode`)
				VALUES ('element_styles','$element_style','1')");
			$switch=$rm->insert_id;
			mysqli_query($rm,"INSERT INTO test_switches
				(`test_id`,`switch_id`)
				VALUES ('$test_id','$switch')");
				
		} elseif ($decision['action']=="change") {
			//style
			$style_q=mysqli_query($db,"SELECT properties.property, styles.value, styles.property_id
				FROM styles
				LEFT OUTER JOIN properties ON styles.property_id = properties.id
				WHERE styles.id ='{$decision['from']}'");
			$style=$style_q->fetch_array();
			//should the (property) of (element) be (value) instead of (old value)
			echo "<div id=test_description hidden >should the <span class=property>".$style['property']."</span> of ".$el_name." be <span class=value>".values_css($decision['to'])."</span> instead of <span class='value old' >".$style['value']."</span>?</div>";
			// check if exists
			$check_q=mysqli_query($db,"SELECT * FROM styles WHERE property_id='{$style['property_id']}' AND value='".values_css($decision['to'])."'");
			$checked=false;
			while ($check=$check_q->fetch_assoc()) { $checked=$check['id'];}
			// to bionic
			if (! $checked) {
				// else INSERT style
				mysqli_query($db,"INSERT INTO styles
					(`property_id`,`value`)
					VALUES ('{$style['property_id']}','".values_css($decision['to'])."')");
				$checked=$db->insert_id;
			}
			// INSERT element_style
			mysqli_query($db,"INSERT INTO element_styles
				(`element_id`,`style_id`)
				VALUES ('{$decision['element']}','$checked')");
			$element_style=$db->insert_id;
			// to _memory
			// INSERT switch
			mysqli_query($rm,"INSERT INTO switches (`table_name`,`table_id`,`mode`)
				VALUES ('element_styles','$element_style','1')");
			$switch=$rm->insert_id;
			// INSERT negative
			$element_style_n_q=mysqli_query($db,"SELECT id FROM element_styles WHERE element_id='{$decision['element']}' AND style_id='{$decision['from']}'");
			$element_style_n_r=$element_style_n_q->fetch_assoc();
			$element_style_n=$element_style_n_r['id'];
			mysqli_query($rm,"INSERT INTO switches (`table_name`,`table_id`,`mode`)
				VALUES ('element_styles','$element_style_n','-1')");
			$n_switch=$rm->insert_id;
			mysqli_query($rm,"INSERT INTO test_switches
				(`test_id`,`switch_id`)
				VALUES ('$test_id','$switch')");
			mysqli_query($rm,"INSERT INTO test_switches
				(`test_id`,`switch_id`)
				VALUES ('$test_id','$n_switch')");
		} elseif ($decision['action']=="remove") {
			$style_q=mysqli_query($db,"SELECT properties.property, styles.value
				FROM styles
				LEFT OUTER JOIN properties ON styles.property_id = properties.id
				WHERE styles.id ='{$decision['style']}'");
			$style=$style_q->fetch_array();
			
			//does the (property) of (element) have to be (value)
			echo "<div id=test_description hidden >does the <span class=property>".$style['property']."</span> of ".$el_name." have to be <span class=value>".values_css($style['value'])."</span>?</div>";
			// get switch from default
			$element_style_n_q=mysqli_query($db,"SELECT id FROM element_styles WHERE element_id='{$decision['element']}' AND style_id='{$decision['style']}'");
			$element_style_n_r=$element_style_n_q->fetch_assoc();
			$element_style_n=$element_style_n_r['id'];
			// to _memory
			// INSERT negative switch
			mysqli_query($rm,"INSERT INTO switches (`table_name`,`table_id`,`mode`)
				VALUES ('element_styles','$element_style_n','-1')");
			$n_switch=$rm->insert_id;
			mysqli_query($rm,"INSERT INTO test_switches
				(`test_id`,`switch_id`)
				VALUES ('$test_id','$n_switch')");
		}
		return $test_id;
	}
	
	// generating defaults
	/* $el_style_q=mysqli_query($db,"SELECT * FROM element_styles");
	while($el_style=$el_style_q->fetch_assoc()) {
		$switch=mysqli_query($rm,"INSERT INTO switches (`table`,`table_id`)
			VALUES ('element_styles','{$el_style['id']}')");
		$id=$rm->insert_id;
		mysqli_query($rm,"INSERT INTO test_switches (`test_id`,`switch_id`)
		VALUES ('1','$id')");
	} */
	
	// testing randomness
	/* $rs=array(0,0);
	for($o=0;$o<10000;$o++){
		$rs[random_decision(array(1,1))]++;
	}
	pre_dump($rs); */
?>
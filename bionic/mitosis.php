<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');
include 'php/funct.php';
function recurse_copy($source, $dest)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest);
    }

    // Loop through the folder
    $dir = dir($source);
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        recurse_copy("$source/$entry", "$dest/$entry");
    }

    // Clean up
    $dir->close();
    return true;
}
recurse_copy('./','../bionic_clone/');
// if (is_dir('php_n/')){
// 	$files = glob('php_n/*'); // get all file names
// 	pre_dump($files);
// 	foreach($files as $file){ // iterate files
// 	  if(is_file($file))
// 	    unlink($file); // delete file
// 	}
// } else {
// 	mkdir('php_n/');
// }

// // clone php
// $files = scandir('php/');
// foreach($files as $file) {
//   //do your work here
//   if (is_file('php/'.$file)) {
//   	echo "\nphp/$file";
//   	echo "\nphp_n/$file";
//   	copy('php/'.$file,'php_n/'.$file);
//   }
// }
// $files = glob('php_n/*.zip');
// foreach($files as $file){
// 	$zip = new ZipArchive;
// 	$zip->open($file);
// 	$zip->extractTo('php_n/');
// 	$zip->close();
// }
?>
import difflib
parts={}
#http://answers.yahoo.com/question/index?qid=20091213142629AAYGkuX
parts['ya']=[
	'centrioles','cilia','flagella','golgi apparatus',
	'lysosomes','microfilaments','microtubules','mitochondria',
	'nucleus','peroxisomes','plasma membrane','ribosomes']
#http://bioh.wikispaces.com/List+of+Organelles
parts['bioh']=[
	'mitochondria','rough endoplasmic reticulum','nucleus','nuclear pores',
	'plasma membrane','nucleolus','nuclear envelope', 'chromatin',
	'smooth endoplasmic reticulum','ribosomes','cilia','golgi apparatus',
	'microtubules','centrioles','peroxisome','lysosome',
	'microfilaments']
#http://www.ivy-rose.co.uk/Biology/Organelles/Organelle-Functions.php
parts['ivy']=[
	'Nucleus','Rough Endoplasmic Reticulum','Smooth Endoplasmic Reticulum','Mitochondria',
	'Chloroplasts','Golgi Apparatus','Lysosomes','Peroxisomes',
	'Secretory vesicles','Vacuole','Ribosomes','Microfilaments',
	'Microtubules','Intermediate Filaments','Junctions','Centrosomes',
	'Cilia','Flagella'
	]
print(parts)
masterparts=[]
for P in parts:
	part=parts[P]
	for p in part:
		if p.lower() not in masterparts and p.lower()+'s' not in masterparts:
			masterparts.append(p.lower())
print(len(masterparts))
masterparts.sort()
for part in masterparts:
	print("'"+part+"':[\"\"],")
#http://biology.unm.edu/ccouncil/Biology_124/Summaries/Cell.html
masterparts={
'centrioles':["Centrioles are self-replicating organelles made up of nine bundles of microtubules and are found only in animal cells. They appear to help in organizing cell division, but aren't essential to the process. "," A structure in an animal cell composed of cylinders of microtubule triplets arranged in a 9 - 0 pattern. An animal cell usually has a pair of centrioles that create spindles used by chromatin in cell division"],
'centrosomes':["Contain the centrioles, which are involved in the process of mitosis - see cell-division."],
'chloroplasts':["An organelle found only in plants and photosynthetic protists that absorbs sunlight and uses it to drive the synthesis of organic compounds from carbon dioxide and water. It contains thick fluid called stroma as well as disks called granum, which is where solar energy is actually trapped.","Chloroplasts are the sites of photosynthesis within plant cells."],
'chromatin':[""],
'cilia':["For single-celled eukaryotes, cilia and flagella are essential for the locomotion of individual organisms. In multicellular organisms, cilia function to move fluid or materials past an immobile cell as well as moving a cell or group of cells. ","Organelles on the outside of a cell. Also contain microtubules which move and propel the cell. There are several on the outside of the cell.","""Some eukaryotic cells have cilia (plural, singular word = cilium) whose function is often to facilitate either movement of the cell or movement of something over the surface of cells e.g. fallopian cells move ova towards the uterus."""],
'flagella':["An organelle on the outside of a cell. It contains microtubules which sway back and forth to move the cell. Usually very few.","The main function of the flagellum of a human spermatozoon (sperm cell) is to enable the sperm to move close to the oocyte ('egg' cell) and orient itself appropriately ."],
'golgi apparatus':["The Golgi apparatus is the distribution and shipping department for the cell's chemical products. It modifies proteins and fats built in the endoplasmic reticulum and prepares them for export to the outside of the cell. ","An organelle in eukaryotic cells consisting of stacks of flat membranous sacs that modify, store, and route products of the endoplasmic reticulum","The Golgi apparatus modifies, sorts and packages macromolecules for delivery to other organelles or secretion from the cell via exocytosis - see (9.) below."],
'intermediate filaments':["""Intermediate filaments are important for maintaining the mechanical structure of cells. There are different types of intermediate filaments that can be identified according to the protein from which they are formed. The different types of intermediate filaments occur in different types of cells and therefore provide structural support (to the cell) in slightly different ways. 
E.g. neurofilaments in the axons of neurons are involved in the radial growth of the axon, so determine its diameter as well as contributing strength and rigidity to the cell."""],
'junctions':["Tight Junctions- Junctions between cells which prevent seepage of any chemicals or fluids; hold cells tightly together; Anchoring Junctions- Junctions which hold cells together and prevent tearing of tissue; Communicating Junction- A junction similar to plasmodesmada; allows communication between cells. (also can be classified as communication)","\"Junctions\" are connecting points joining either cells to other cells, or cells to their basement membrane. See the diagram of the cytoskeleton."],
'lysosomes':["The main function of these microbodies is digestion. Lysosomes break down cellular waste products and debris from outside the cell into simple compounds, which are transferred to the cytoplasm as new cell-building materials. "," A membrane-enclosed bag of hydrolytic enzymes found in the cytoplasm of eukaryotic cells, destroys harmful bacteria, recycle damaged organelles, help develop embryos, expose nutrients to digestive enzymes. Analogy: scissors.","""Lysosomes (tiny sacs containing enzymes) are the main sites of intracellular digestion. They enable the cell to make use of nutrients. Their functions can be listed as:

Autophagy - digestion of materials from within the cell.
Heterophagy - digestion of materials originating from outside the cell.
Biosynthesis - recycling unwanted products of chemical reactions to process materials received from outside the cell.
Lysosomes also destroy the cell - usually after it has died."""],
'microfilaments':["Microfilaments are solid rods made of globular proteins called actin. These filaments are primarily structural in function and are an important component of the cytoskeleton. ","""Actin has a contractile function in muscle cells.

In non-muscle cells actin microfilaments form part of a web-like layer (called the cell cortex) located immediately below the cell's plasma membrane. This structure helps to define the shape of the cell including the structure of any microvilli. They also facilitate movement of certain particles and structures e.g. macrophages, fibroblasts and nerve growth cones."""],
'microtubules':["These straight, hollow cylinders, composed of tubulin protein, are found throughout the cytoplasm of all eukaryotic cells and perform a number of functions. ","""As the main "building blocks" forming the cytoskeleton - the cell's framework within which all components of the cell are held in position or allowed restricted movement.
Movement of materials and structures within cells e.g. help form the miotic spindle during the "prophase" part of cell division by mitosis."""],
'mitochondria':["Mitochondria are oblong shaped organelles that are found in the cytoplasm of every eukaryotic cell. In the animal cell, they are the main power generators, converting oxygen and nutrients into energy. ","An organelle in eukaryotic cells that serves as the site of cellular respiration. Consists of two membranes. Inside the second there are folds, called cristae, which increase the surface area of the membrane and enhance the production of ATP.","The main function of mitochondria in aerobic cells is the production of energy by synthesis of ATP. However, mitochondria also have many other functions, including e.g.:","""Processing and storage of calcium ions (Ca2+).
Apoptosis, i.e. the process of programmed cell death
Regulation of cellular metabolism
Synthesis of certain steroids """],
'nuclear envelope':["double membrane of the nucleus that encloses genetic material in eukaryotic cells, separating the contents of the nucleus (particularly DNA) from the cytosol"],
'nuclear pores':["Holes in the nuclear membrane that allow the passing of RNA in and out of the nucleus"],
'nucleolus':["produces ribosomes in nucleus."],
'nucleus':["The nucleus is a highly specialized organelle that serves as the information and administrative center of the cell. ","The chromosome-containing organelle of a eukaryotic cell. Controls cell. (Could also be put in communication category)",""""Control Center" of the cell. 
"Contains the cell's DNA (genetic information) in the form of genes."""],
'peroxisomes':["Microbodies are a diverse group of organelles that are found in the cytoplasm, roughly spherical and bound by a single membrane. There are several types of microbodies but peroxisomes are the most common. ", "Turn dangerous peroxide (H2O2) into water (H2O).","""Similar to (but smaller than) lysosomes, the metabolic functions of peroxisomes include:

Breakdown of fatty acids by beta-oxidation
Breakdown excess purines to urea
Breakdown of toxic compounds e.g. in the cells of the liver and kidney.
also play a role in the biosynthesis of certain important molecules incl. cholesterol and (in liver cells) bile acids derived from cholesterol."""],
'plasma membrane':["All living cells have a plasma membrane that encloses their contents. In prokaryotes, the membrane is the inner layer of protection surrounded by a rigid cell wall. Eukaryotic animal cells have only the membrane to contain and protect their contents. These membranes also regulate the passage of molecules in and out of the cells. ","A delicate membrane made of lipids surrounding cells that controls what goes in and out of the cell"],
'ribosomes':["All living cells contain ribosomes, tiny organelles composed of approximately 60 percent RNA and 40 percent protein. In eukaryotes, ribosomes are made of four strands of RNA. In prokaryotes, they consist of three strands of RNA.","A cell organelle constructed in the nucleolus and functioning as the site of protein synthesis in the cytoplasm; consists of RNA and protein molecules, which make up two subunits. Synthesizes proteins in the Rough Endoplasmic Reticulum. (Could also be put in the communication category)","Biosynthesis of ribosomal RNA (rRNA) and production (assembly) of ribosomes.","""Produce polypeptides that are then either ...

inserted into the RER membrane, or
moved into the lumen (central region) of the cisternae, or
moved to the Golgi complex and probably onwards from there.""","Ribosomes interpret cellular information from the nucleus and synthesize proteins. There are different types of ribosomes e.g. 80S (eukaryotic), 70S (prokaryotic)."],
'rough endoplasmic reticulum':["The endoplasmic reticulum is a network of sacs that manufactures, processes, and transports chemical compounds for use inside and outside of the cell. It is connected to the double-layered nuclear envelope, providing a connection between the nucleus and the cytoplasm. ","portion of the endoplasmic reticulum studded with ribosomes that synthesizes proteins. Also transports the proteins made in the ribosomes.","Consists of many interconnected membranous sacs called cisternae, onto whose external surface ribosomes are attached (distinguishing RER from SER on electron micrographs)."],
'secretory vesicles':["A small, membranous sac that can be used for transportation (Could also be put in communication category).","""Transport and delivery of their contents (e.g. molecules such as hormones or neurotransmitters) either into or out of the cell, in both cases via the cell membrane.

Exocytosis - movement of the contents of secretory vesicles out of the cell.
Endocytosis - movement of the contents of secretory vesicles into the cell."""],
'smooth endoplasmic reticulum':["portion of the endoplasmic reticulum that is free of ribosomes that synthesis lipids, stores calcium ions, and detoxifies in the liver.","""Consists of many interconnected membranous sacs called cisternae (without ribosomes). 
Many enzymes are either attached to the surface of the SER or located within its cisternae."""],
'vacuole':["A membrane-enclosed sac taking up most of the interior of a mature plant cell and containing a variety of substances important in plant reproduction, growth, and development. It also provides support for a plant. When vacuoles lose water, a plant will wilt (Vacuoles could also be put in Breakdown because of the enzymes they contain).","Helps maintain turgor pressure pressure (turgidity) inside the cell - which pushes the plasma membrane against the cell wall. Plants need turgidity to maintain rigidity."],
'cytoskeleton':["A network of microtubules, microfilaments, and intermediate filaments that branch throughout the cytoplasm and serve a variety of mechanical and transport functions."]
	}
tidlparts={
'intermediate filaments':"Maintain the mechanical structure of the cell, providing rigidity. Filaments in diferent types of cells are formed from diferent protiens.",
'peroxisomes':"Peroxisomes are the most common type of microbody (not to be confused with a microbe), they clean the cell of toxins such as peroxide and break down fatty acids.",
'microfilaments':"Microfilaments are an important part of the cytoskeleton, they form a web-like layer below the cell's plasma membrane. It helps to define the shape of the cell.",
'lysosomes':"Lysosomes digest materials both within and originating from outside the cell, they recycle by-products of internal chemical reactions to do this.",
'cytoskeleton':"A network of microtubules, microfilaments, and intermediate filaments that branch throughout the cytoplasm and serve a variety of mechanical and transport functions.",
'ribosomes':"A ribosome is made from strands of RNA and protiens. They are the site of protein synthesis in the cytoplasm, converting RNA into Protiens.",
'plasma membrane':"The plasma membrane is a layer of lipids that controlls what goes in and out of the cell. ",
'cilia':"Hair-like structures that help to move the cell, or move the suuroundings of the cell away from it.",
'chloroplasts':"Chloroplasts trap energy from sunlight in organic mollecules made of carbon dioxide and water.",
'golgi apparatus':"The Golgi Apparatus modifies macromolecules (protiens & fats) built in the Endoplasmic Reticulum for transfer outside of the cell or to other organelles.",
'secretory vesicles':"An enclosed sac that is used to transport molecules through the membrane. Cells may recieve secretory vesicles from other cells through the plasma membrane.",
'flagella':"A small collection of microtubules outside of the cell that enable it to move.",
'vacuole':"An enclosed sac which provides pressure inside of a plant cell to keep the cell wall rigid.",
'nuclear pores':"Holes in the nuclear membrane that allow the passing of RNA in and out of the nucleus.",
'junctions':"Junctions are part of the cytoskeleton, they connect cells to other cells - or simply to their basement membrane. Communicating junctions allow communication of molecules between cells, whereas tight junctions prevent seepage.",
'centrosomes':"Contain the centrioles, which are involved in the process of cell division.",
'chromatin':"null",
'centrioles':"Self-replicating organelles found in animal cells that help with cell division. They contain nine bundles of microtubules.",
'nucleolus':"Produces ribosomes",
'nucleus':"The information and administritive center of the cell, contains all the information required to build the cell in the form of genes.",
'microtubules':"Straight, hollow cylinders that form the main building blocks of the cytoskeleton - restricting the movement of the cells componetents but facilitating the movement of materials.",
'nuclear envelope':"Double-membrane that separates the DNA from the cytosol in the nucleus.",
'rough endoplasmic reticulum':"Endoplasmic reticulum that is studded with ribosomes. It also transports the protiens made in the ribosomesand connects the nuclear envelope to the cytoplasm.",
'mitochondria':"Mitochondia generate energy by respiration of oxygen and nutrients in the form of ATP. They have other functions including synthesis of steroids and storage of calcium ions.",
'smooth endoplasmic reticulum':"Endoplasmic rectilium that has no ribosomes. It synthesises lipids, stores calcium ions and many enzymes are attached to its surface."
}
print('<table>')
for part in tidlparts:
	if tidlparts[part]!='null':
		print('\t<tr>')
		print('\t\t<td>'+part+'</td>')
		print('\t\t<td>'+tidlparts[part]+'</td>')
		print('\t</tr>')
print('</table>')
